package com.turner.loki.plugins;


import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.Calendar;
import java.util.Hashtable;

import javax.jms.JMSException;
import javax.jms.Message;

import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.log4j.Logger;

import com.turner.loki.AbstractPlugin;
import com.turner.loki.GenericMessage;
import com.turner.loki.core.exceptions.InitializationException;
import com.turner.loki.core.exceptions.PluginException;

public class LmdbLoki extends AbstractPlugin {
	private static Logger logger = Logger.getLogger(LmdbLoki.class);
	protected static final SimpleDateFormat URL_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
	private String urlNow;
	private String urlLater;
	private GregorianCalendar siteStartCalendar;
	private GregorianCalendar siteEndCalendar;
	boolean calStartGTSiteStart = false;
	boolean calStartGTSiteEnd = false;
	boolean calEndGTSiteStart = false;
	boolean calEndGTSiteEnd = false;
	private String liveFeedVersion = "1.0";
	
	// From initialize
	private int publishWindowStartOffset;
	private int publishWindowEndOffset;
	private String startOfDay;
	private String endOfDay;
	private boolean siteStartBeforeEnd;
	private boolean showDayRequired = false;
	private String network = null;
	private String streamingFeedUrl;
	private String streamingFeedUser;
	private String streamingFeedPw;
	
	// End From initialize
	
	@Override
	public void initialize(Hashtable<String, String> properties) throws InitializationException {
		super.initialize(properties);
		logger.info("Init called: " + properties);
		
		if(properties.containsKey("publishWindowStartOffset")) {
			this.publishWindowStartOffset = Integer.valueOf(properties.get("publishWindowStartOffset"));
		} else {
			throw new InitializationException("No publishWindowStartOffset");
		}
		if(properties.containsKey("publishWindowEndOffset")) {
			this.publishWindowEndOffset = Integer.valueOf(properties.get("publishWindowEndOffset"));
		} else {
			throw new InitializationException("No publishWindowEndOffset");
		}
		if(properties.containsKey("startOfDay")) {
			this.startOfDay = properties.get("startOfDay");
		} else {
			throw new InitializationException("No startOfDay");
		}
		if(properties.containsKey("endOfDay")) {
			this.endOfDay = properties.get("endOfDay");
		} else {
			throw new InitializationException("No endOfDay");
		}
		if(properties.containsKey("siteStartBeforeEnd")) {
			this.siteStartBeforeEnd = Boolean.getBoolean(properties.get("siteStartBeforeEnd"));
		} else {
			throw new InitializationException("No siteStartBeforeEnd");
		}
		if(properties.containsKey("showDayRequired")) {
			this.showDayRequired = Boolean.getBoolean(properties.get("showDayRequired"));
		} else {
			throw new InitializationException("No showDayRequired");
		}
		if(properties.containsKey("network")) {
			this.network = properties.get("network");
		} else {
			throw new InitializationException("No network");
		}
		if(properties.containsKey("streamingFeedUrl")) {
			this.streamingFeedUrl = properties.get("streamingFeedUrl");
		} else {
			throw new InitializationException("No streamingFeedUrl");
		}
		if(properties.containsKey("streamingFeedUser")) {
			this.streamingFeedUser = properties.get("streamingFeedUser");
		} else {
			throw new InitializationException("No streamingFeedUser");
		}
		if(properties.containsKey("streamingFeedPw")) {
			this.streamingFeedPw = properties.get("streamingFeedPw");
		} else {
			throw new InitializationException("No streamingFeedPw");
		}
	}
	
	@Override
	public Object process(Message message) throws PluginException {
		super.process(message);
		
		GenericMessage gm = (GenericMessage) message;
		String xmlString = doLiveFeed();
		
		doBvi(xmlString);
		

		try {
			gm.setStringProperty("body", xmlString);
		} catch (JMSException e) {
			logger.error("JMSException setting string property: " + e.getLocalizedMessage());
			throw new PluginException("JMS Exception setting string property: " + e.getLocalizedMessage(),e);
		}
		
		
		return gm;
		
	}
	
	private void doBvi(String xmlString) {
		logger.info("Enter into doBvi");
		
		// Send a bvi if needed
		String[] providerArray = xmlString.split("tveLiveSched Provider=\""); 
		logger.debug("providerArray length: " + providerArray.length);

		// Handle the airings for this provider
		for(int i = 1; i < providerArray.length; i++) {
			logger.debug("providerArray " + i + " : " + providerArray[i]);
			
			// extract the provider name from the feed
			int endOfFeedProvider = providerArray[i].indexOf(34);
			String feedProvider = providerArray[i].substring(0, endOfFeedProvider);
			logger.info("feedProvider: " + feedProvider);

			if(feedProvider.equalsIgnoreCase(this.network)) {
				logger.info("found provider: " + this.network);
		
				// split on SchedItem AiringID="
				String[] dataArray = providerArray[i].split("SchedItem AiringID=\"");
				logger.info("providerArray length: " + dataArray.length);
		
				for (int j = 1; j < dataArray.length; j++) {
					processOneAiring(dataArray[j]);
				}
				break;
			} else {
					logger.info("Provider did not match: " + this.network);
			}
		}
		logger.debug("Finished a feed"); 
		return;
	}
	
	/**
	 * processOneAiring - Send a bvi.xml for an airing (if needed)
	 * 		1. Instantiate an Airing.
	 * 		2. Ask the Airing to process itself.
	 * 
	 * @param airingString - a String for a single airing as extracted from the feed
	 * @return - void
	 */
	private void processOneAiring(String airingString) {
		logger.info("Enter into processOneAiring: " + airingString);
		String titleId = null;
		String franchiseId = null;
		String airingId = null;
		String programType = null;
		String sportsName = null;
		
		if(airingString.indexOf("</TitleID>")!= -1) {
			titleId = airingString.substring(airingString.indexOf("<TitleID>") + 9, airingString.indexOf("</TitleID>"));
		}
		if(airingString.indexOf("</FranchiseID>")!= -1) {
			franchiseId = airingString.substring(airingString.indexOf("<FranchiseID>") + 13, airingString.indexOf("</FranchiseID>"));
		}
		if(airingString.indexOf("\"") != -1) {
			airingId = airingString.substring(0, airingString.indexOf("\""));
		}
		if(airingString.indexOf("</ProgramType>") != -1) {
			programType = airingString.substring(airingString.indexOf("<ProgramType>") + 13, airingString.indexOf("</ProgramType>"));
		} 
		if(airingString.indexOf("</SportsName>") != -1) {
				sportsName = airingString.substring(airingString.indexOf("<SportsName>") + 12, airingString.indexOf("</SportsName>"));
		} 
		String registerBviUrl = "http://teg8devapp1.turner.com:9196/cma/cvpData/airing.do?titleId=" + titleId + 
								"&franchiseId=" + franchiseId + "&airingId=" + airingId;

		logger.info("Feed Url: " + registerBviUrl);
		HttpClient client = new HttpClient();
		//client.getState().setCredentials(AuthScope.ANY, defaultcreds);
		GetMethod get = new GetMethod(registerBviUrl);
		String response = null;
		try {
			int status = client.executeMethod(get);
			logger.info("status: " + status);
			response = get.getResponseBodyAsString();
			logger.info("response size: " + response.length());		 
		} catch(Exception ex) {
			logger.error("Exception in client.executeMethod(get) : ", ex);
		} finally {
			get.releaseConnection();
		}

	}
	
	
	
	
	
	/**
	 * doLiveFeed
	 * 		1. Build the dates start and end date for the live stream using the Spring provided parameters 
	 * 			(http://livefeed.apphb.com/api/airing?start="+urlNow+"&end="+urlLater)
	 * 		2. Instantiate an "HTTP Client" and setup the user and password.
	 * 		3. Instantiate a GetMethod for the URL
	 * 		4. Execute the GetMethod on the "HTTP CLient".
	 * 		5. Split the xml returned by "Provider" into a String array
	 * 		5. For each of the "Providers" (TOON, TOONHD, ...) passed in by Spring
	 * 			1. Find the xml array for the active "Provider"
	 * 			2. Split the xml array on AiringId into an xml data array
	 * 			3. Give the data array to the "processOneAiring" method.
	 * 
	 */
	
	private String doLiveFeed() {
		// Setup the requested time period for the URL
		logger.info("Enter into doLiveFeed");
		
		
		setFeedTimes();
		logger.debug("urlNow: " + getFeedStartTime() + ", urlLater: " + getFeedEndTime());

		if(getFeedStartTime() == null || getFeedEndTime() == null) {
			logger.info("Null Snippet");
			return "<?xml version='1.0' encoding='utf-8'?><LiveFeed version=" + this.liveFeedVersion + " />";
		} else {
			// Get the feed
			String url = streamingFeedUrl + "?start=" + getFeedStartTime() 
							+ "&end=" + getFeedEndTime() + "&network=" + this.network + "&timezone=EST";
			logger.info("Feed Url: " + url + ", " + this.streamingFeedUser + ", " + this.streamingFeedPw);
			HttpClient client = new HttpClient();
		
			client.getParams().setAuthenticationPreemptive(true);
			Credentials defaultcreds = new UsernamePasswordCredentials(this.streamingFeedUser, this.streamingFeedPw);
			client.getState().setCredentials(AuthScope.ANY, defaultcreds);
			GetMethod get = new GetMethod(url);		 

			// Get the xml file
			String response = null;
			try {
				int status = client.executeMethod(get);
				logger.info("status: " + status);
				response = get.getResponseBodyAsString();
				logger.info("response size: " + response.length());		 
			} catch(Exception ex) {
				logger.error("Exception in client.executeMethod(get) : ", ex);
			} finally {
				get.releaseConnection();
			}

			// Did the feed return a usable amount of data
			if(response != null && response.length() > 60) {
				// Publish the xml snippet!
				String fileName = this.network + ".xml";
				// Cartoon and Adult Swim share the same lmdb feed provider (TOONHD). Handle special code 
				response = getActiveResponse(response);
				logger.info("Returned from getActiveResponse: " + response.length());

				// Save the snippet if required
				logger.info("Publish the xml file for CVP: " + fileName);
				logger.debug("xml file: " + response);
				return response; 
			} else if(response != null) {
				logger.info("Feed return is to small: " + response.length());
				return "<?xml version='1.0' encoding='utf-8'?><LiveFeed version=" + this.liveFeedVersion + " />";
			} else {
				logger.info("Feed return is null:");
				return "<?xml version='1.0' encoding='utf-8'?><LiveFeed version=" + this.liveFeedVersion + " />";
			}
		}
	}	
	
	
	/**
	 * getActiveResponse - Over-ridable method to allow Cartoon and Adult Swim to filter the lmdb feed based on their activity times.
	 * 
	 * This method is the nominal case, It returns the original string without modification
	 * 
	 * @param response - the xml returned by the lmdb feed
	 * @return - the xml file filtered to remove airings not associated with the Site
	 */
	protected String getActiveResponse(String response) {
		logger.info("Enter into core getActiveResponse");
		return response;
	}	
	
	
	
	
	
	/**
	 * setFeedTimes - setup the times to be used when recovering the lmdb feed
	 * 
	 */
	public void setFeedTimes() {
		logger.info("Enter into setFeedTimes: " + this.network);
		
		this.calStartGTSiteStart = false;
		this.calStartGTSiteEnd = false;
		this.calEndGTSiteStart = false;
		this.calEndGTSiteEnd = false;
		
		
		GregorianCalendar now = new GregorianCalendar();
		GregorianCalendar later = new GregorianCalendar();		
		now.add(Calendar.HOUR_OF_DAY, this.publishWindowStartOffset); 
		later.add(Calendar.HOUR_OF_DAY, this.publishWindowEndOffset);
		
		if(!showDayRequired) {
			logger.info("Standard Feed times: " + this.publishWindowStartOffset + "," + this.publishWindowEndOffset);
			this.urlNow = URL_DATE_FORMAT.format(now.getTime());
			this.urlLater = URL_DATE_FORMAT.format(later.getTime());
		} else {
			logger.info("showDay Testing Required: " + this.startOfDay + "," + this.endOfDay);
			logNowLaterCalendars(now, later);
			
			buildSiteHoursCalendar();
			
			// calendar comparison for Cartoon
			if(this.siteStartBeforeEnd) {
				if(now.compareTo(this.siteStartCalendar) > 0) {
					this.calStartGTSiteStart = true;
				} 
				if(now.compareTo(this.siteEndCalendar) > 0) {
					this.calStartGTSiteEnd = true;
				}
				if(later.compareTo(this.siteStartCalendar) > 0) {
					this.calEndGTSiteStart = true;
				}
				if(later.compareTo(this.siteEndCalendar) > 0) {
					this.calEndGTSiteEnd = true;
				} else {
					this.calEndGTSiteEnd = false;
				}
			} 
			// calendar comparison for Adult Swim
			else {
				if(now.compareTo(this.siteStartCalendar) > 0) {
					this.calStartGTSiteStart = true;
				} 
				if(now.compareTo(this.siteEndCalendar) > 0) {
					this.calStartGTSiteEnd = true;
				}
				if(later.compareTo(this.siteStartCalendar) > 0) {
					this.calEndGTSiteStart = true;
				}
				// Is the calendar end tomorrow?
				if(later.get(Calendar.DATE) > this.siteStartCalendar.get(Calendar.DATE) ) {
					logger.debug("Calendar end is next day: " + later.get(Calendar.DATE) + ", " 
							+ this.siteStartCalendar.get(Calendar.DATE));
					later.add(Calendar.DATE, -1);
				}
				if(later.compareTo(this.siteEndCalendar) > 0) {
					this.calEndGTSiteEnd = true;
					//logNowLaterCalendars(now, later);			
					//logSiteCalendars();
					//logger.debug("calEndGTSiteEnd: " + this.calEndGTSiteEnd);
				} else {
					this.calEndGTSiteEnd = false;
					//logNowLaterCalendars(now, later);			
					//logSiteCalendars();
					//logger.debug("calEndGTSiteEnd: " + this.calEndGTSiteEnd);
				}

			}
			logger.debug("calStartGTSiteStart: " + this.calStartGTSiteStart + ", calStartGTSiteEnd: " + this.calStartGTSiteEnd);
			logger.debug("calEndGTSiteStart: " + this.calEndGTSiteStart + ", calEndGTSiteEnd: " + this.calEndGTSiteEnd);
			
			//Cartoon
			if(this.siteStartBeforeEnd) {
				logger.debug("siteStartBeforeEnd is true; Cartoon");
				boolean clipStart = false;
				boolean clipEnd = false;
				this.urlNow = URL_DATE_FORMAT.format(now.getTime());
				this.urlLater = URL_DATE_FORMAT.format(later.getTime());

				if(!this.calStartGTSiteStart && !this.calStartGTSiteEnd
					|| this.calStartGTSiteStart && this.calStartGTSiteEnd) {
					logger.debug("snippet period start needs to be clipped");
					this.urlNow = URL_DATE_FORMAT.format(this.siteStartCalendar.getTime());
					clipStart = true;
				}
				if(!this.calEndGTSiteStart && !this.calEndGTSiteEnd
					|| this.calEndGTSiteStart && this.calEndGTSiteEnd) {
					logger.debug("snippet period end needs to be clipped");
					this.urlLater = URL_DATE_FORMAT.format(this.siteEndCalendar.getTime());
					clipEnd = true;
				}
				if(clipStart && clipEnd) {
					this.urlNow = null;
					this.urlLater = null;
					logger.debug("No Snippet");
				} else if(!clipStart && !clipEnd) {
					logger.debug("No clip needed");
				}
			} 
			// Adult Swim
			else {
				logger.debug("siteStartBeforeEnd is false; Adult Swim");
				boolean clipStart = false;
				boolean clipEnd = false;
				this.urlNow = URL_DATE_FORMAT.format(now.getTime());
				this.urlLater = URL_DATE_FORMAT.format(later.getTime());
				
				if(!this.calStartGTSiteStart && this.calStartGTSiteEnd) {
					logger.debug("snippet period start needs to be clipped");
					this.urlNow = URL_DATE_FORMAT.format(this.siteStartCalendar.getTime());
					clipStart = true;
				}
				if(!this.calEndGTSiteStart && this.calEndGTSiteEnd) {
					logger.debug("snippet period end needs to be clipped");
					this.urlLater = URL_DATE_FORMAT.format(this.siteEndCalendar.getTime());
					clipEnd = true;
				}
				if(clipStart && clipEnd) {
					this.urlNow = null;
					this.urlLater = null;
					logger.debug("No Snippet");
				} else if(!clipStart && !clipEnd) {
					logger.debug("No clip needed");
				}
			}
		} 
		logger.debug("urlNow: " + this.urlNow + ", urlLater: " + this.urlLater);
	}
	
	private void buildSiteHoursCalendar() {
		logger.info("Enter into buildSiteHoursCalendar: " + this.startOfDay + "," + this.endOfDay + ", " + this.siteStartBeforeEnd);
		
		// Get the site start times
	    int iStartHour = Integer.parseInt(this.startOfDay.substring(0,2));
	    int iStartMinute = Integer.parseInt(this.startOfDay.substring(3, 5));
	    logger.debug("startHour: " + iStartHour + ", startMinute: " + iStartMinute);
	    
	    // Get the site end tiimes
	    int iEndHour = Integer.parseInt(this.endOfDay.substring(0,2));
	    int iEndMinute = Integer.parseInt(this.endOfDay.substring(3, 5));
	    logger.debug("endHour: " + iEndHour + ", endMinute: " + iEndMinute);
		
		this.siteStartCalendar = new GregorianCalendar();
		this.siteEndCalendar = new GregorianCalendar();

		// Set the hour and minute for the Start of Site
		this.siteStartCalendar.set(Calendar.HOUR_OF_DAY, iStartHour);
		this.siteStartCalendar.set(Calendar.MINUTE, iStartMinute);
		this.siteStartCalendar.set(Calendar.SECOND, 0);
		this.siteStartCalendar.getTime();
		
		// Set the hour and minute for the End of Site
		this.siteEndCalendar.set(Calendar.HOUR_OF_DAY, iEndHour);
		this.siteEndCalendar.set(Calendar.MINUTE, iEndMinute);
		this.siteEndCalendar.set(Calendar.SECOND, 0);
		logSiteCalendars();
		
		if(this.siteStartBeforeEnd) {
			logger.debug("siteStartBeforeEnd is true; Cartoon: " + this.siteStartCalendar);
			//this.siteStartCalendar.set(Calendar.AM_PM, Calendar.AM);
			this.siteStartCalendar.getTime();
			this.siteEndCalendar.set(Calendar.AM_PM, Calendar.PM);
			this.siteEndCalendar.getTime();

		} else {
			logger.debug("siteStartBeforeEnd is false; Adult Swim");
			this.siteStartCalendar.set(Calendar.AM_PM, Calendar.PM);
			//this.siteEndCalendar.add(Calendar.DATE, 1);
			this.siteEndCalendar.set(Calendar.AM_PM, Calendar.AM);
		}
		logger.debug("Exit from buildSiteHoursCalendar");
		logSiteCalendars();
	}
	
	private void logNowLaterCalendars(Calendar now, Calendar later) {
		logger.debug("Now Calendar (now): AM_PM: " + now.get(Calendar.AM_PM)
				+ ", DATE: " + now.get(Calendar.DAY_OF_MONTH)
				+ ", HOUR: " + now.get(Calendar.HOUR)
				+ ", HOUR_OF_DAY: " + now.get(Calendar.HOUR_OF_DAY)
				+ ", MINUTE: " + now.get(Calendar.MINUTE)
				+ ", SECOND: " + now.get(Calendar.SECOND));
		logger.debug("Later Calendar (later): AM_PM: " + later.get(Calendar.AM_PM)
				+ ", DATE: " + later.get(Calendar.DAY_OF_MONTH)
				+ ", HOUR: " + later.get(Calendar.HOUR)
				+ ", HOUR_OF_DAY: " + later.get(Calendar.HOUR_OF_DAY)
				+ ", MINUTE: " + later.get(Calendar.MINUTE)
				+ ", SECOND: " + later.get(Calendar.SECOND));

	}
	
	private void logSiteCalendars() {
		logger.debug("Site Start Calendar: AM_PM: " + this.siteStartCalendar.get(Calendar.AM_PM)
				+ ", DATE: " + this.siteStartCalendar.get(Calendar.DAY_OF_MONTH)
				+ ", HOUR: " + this.siteStartCalendar.get(Calendar.HOUR)
				+ ", HOUR_OF_DAY: " + this.siteStartCalendar.get(Calendar.HOUR_OF_DAY)
				+ ", MINUTE: " + this.siteStartCalendar.get(Calendar.MINUTE)
				+ ", SECOND: " + this.siteStartCalendar.get(Calendar.SECOND));
		logger.debug("Site End Calendar: AM_PM: " + this.siteEndCalendar.get(Calendar.AM_PM)
				+ ", DATE: " + this.siteEndCalendar.get(Calendar.DAY_OF_MONTH)
				+ ", HOUR: " + this.siteEndCalendar.get(Calendar.HOUR)
				+ ", HOUR_OF_DAY: " + this.siteEndCalendar.get(Calendar.HOUR_OF_DAY)
				+ ", MINUTE: " + this.siteEndCalendar.get(Calendar.MINUTE)
				+ ", SECOND: " + this.siteEndCalendar.get(Calendar.SECOND));

	}
	
	private String getFeedStartTime() {
		logger.info("Enter into getFeedStartTime: " + this.urlNow);
		return this.urlNow;
	}
	private String getFeedEndTime() {
		logger.info("Enter into getFeedEndTime: " + this.urlLater);
		return this.urlLater;
	}

	
	
}
